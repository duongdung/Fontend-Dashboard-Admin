var foodList = [];

(function () {

  // Get the form.
  // var form = $('#ajax-contact');

  // // Get the messages div.
  // var formMessages = $('#form-messages');

  // Set up an event listener for the contact form.
  // $(form).submit(function(e) {
  // 	// Stop the browser from submitting the form.
  // 	e.preventDefault();

  // 	// Serialize the form data.
  // 	var formData = $(form).serialize();

  // 	// Submit the form using AJAX.
  // 	$.ajax({
  // 		type: 'POST',
  // 		url: $(form).attr('action'),
  // 		data: formData
  // 	})
  // 	.done(function(response) {
  // 		// Make sure that the formMessages div has the 'success' class.
  // 		$(formMessages).removeClass('error');
  // 		$(formMessages).addClass('success');

  // 		// Set the message text.
  // 		$(formMessages).text(response);

  // 		// Clear the form.
  // 		$('#name').val('');
  // 		$('#email').val('');
  // 		$('#subject').val('');
  // 		$('#message').val('');
  // 	})
  // 	.fail(function(data) {
  // 		// Make sure that the formMessages div has the 'error' class.
  // 		$(formMessages).removeClass('success');
  // 		$(formMessages).addClass('error');

  // 		// Set the message text.
  // 		if (data.responseText !== '') {
  // 			$(formMessages).text(data.responseText);
  // 		} else {
  // 			$(formMessages).text('Oops! An error occured and your message could not be sent.');
  // 		}
  // 	});

  // });
  getListFoodFromDB();
})();

function getListFoodFromDB() {
  $.ajax({
    headers: {
      accepts: "application/json",
    },
    type: 'GET',
    url: "http://localhost:8080/api/get/foods?name=",
    success: function (result) {
      foodList = [];
      for (let i = 0; i < result.length; i++) {
        if (result[i].activated === 1) {
          foodList.push(result[i]);
        }
      }
      createFoodList(foodList);
    },
    error: function (result, status) {
      console.log(status);
    }

  });
}



function createFoodList(foodList) {
  let option = '<div class="mu-tab-content-left">' +
    '<ul class="mu-menu-item-nav">'
  for (let i = 0; i < foodList.length / 2; i++) {
    option +=
      '<li>' +
      '<div class="media">' +
      '<div class="media-left">' +
      '<a href="#">' +
      '<img class="media-object" src="http://localhost:8080/api/image/food/' + foodList[i].image + '" alt="img">' +
      '</a>' +
      '</div>' +
      '<div class="media-body">' +
      '<h4 class="media-heading"><a href="#">' + foodList[i].name + '</a></h4>' +
      '<span class="mu-menu-price">' + foodList[i].price + ' vnd</span>' +
      '<p>' + foodList[i].description + '</p>' +
      '</div>' +
      '</div>' +
      '</li>'
  }
  option += '</ul></div>';
  document.getElementById('view-list-item-left').innerHTML = option;
  option = '<div class="mu-tab-content-right">' +
    '<ul class="mu-menu-item-nav">';
  for (let i = Math.round(foodList.length / 2); i < foodList.length; i++) {
    option +=
      '<li>' +
      '<div class="media">' +
      '<div class="media-left">' +
      '<a href="#">' +
      '<img class="media-object" src="http://localhost:8080/api/image/food/' + foodList[i].image + '" alt="img">' +
      '</a>' +
      '</div>' +
      '<div class="media-body">' +
      '<h4 class="media-heading"><a href="#">' + foodList[i].name + '</a></h4>' +
      '<span class="mu-menu-price">' + foodList[i].price + ' vnd</span>' +
      '<p>' + foodList[i].description + '</p>' +
      '</div>' +
      '</div>' +
      '</li>'
  }
  option += '</ul></div>';
  document.getElementById('view-list-item-right').innerHTML = option;
}

function searchFood() {
  let search = document.getElementById('text-search').value;
  let searchList = [];
  if (search !== "") {
    for (let index = 0; index < foodList.length; index++) {
      if (foodList[index].name.search(search) != -1) {
        searchList.push(foodList[index]);
      }
    }
    if(searchList.length != 0) {
      createFoodList(searchList);
    }
  }else{
    createFoodList(foodList);
  }
}

