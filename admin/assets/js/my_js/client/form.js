function showForm(id_display, id_hidden) {
    document.getElementById(id_display).style.display = "block";
    document.getElementById(id_hidden).style.display = "none";
}

function login() {
    $.ajax({
        headers: {
            'Content-Type': 'application/json'
        },
        url: "http://localhost:8080/api/check/admin",
        type: 'POST',
        data: JSON.stringify({
            userName: document.getElementById('lg-user-name').value,
            password: document.getElementById('lg-password').value,
        }),
        success: function (status) {
            if (status === 'sucessfull') {
                sucsessFull(status);
            } else {
                alert(status);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    })

}

function register() {
    if (checkSamePass()) {
        $.ajax({
            headers: {
                'Content-Type': 'application/json'
            },
            url: "http://localhost:8080/api/insert/admin",
            type: 'POST',
            data: JSON.stringify({
                "code": document.getElementById('regis-code').value,
                "userName": document.getElementById('regis-user-name').value,
                "password": document.getElementById('regis-password').value,
                "permission": document.getElementById('regis-permission').value,
                "activated": 1
            }),
            success: function (status) {
                if (status === 'sucessfull') {
                    alert(status);
                    sucsessRegister(status);
                } else {
                    alert(status);
                }
            },
            error: function (result, status) {
                console.log(status);
            }
        })
    } else {
        alert("pass Not same");
    }

}

function sucsessFull(status) {
    alert(status);
    window.location.assign('admin.html');
}

function sucsessRegister(status) {
    alert(status);
    showForm('id-form-login', 'id-form-register');
}

function checkSamePass() {
    let pass1 = document.getElementById('regis-password').value;
    let pass2 = document.getElementById('regis-password-repeat').value;
    return pass1 === pass2;
}