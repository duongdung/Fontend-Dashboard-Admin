function showForm(title) {
    document.getElementById("form-title").innerHTML = title;
    title === 'Edit Profile' ? addminMode = "E" : title === 'Add Profile' ? addminMode = "A" : addminMode = "D";
}


function updateProfile() {

    if (addminMode === 'D') {
        showDetail();
    } else if (addminMode === 'E') {
        editProfile();
    } else {
        addNewUser();
    }

}

function showDetail() {
    $.ajax({
        url: "http://localhost:8080/api/get/receptionist?Id=" + receptionistID,
        type: 'GET',
        success: function (result) {
            setDataForForm(result);
        }
    })
}

function addNewUser() {
    
}

function editProfile() {
    console.log(document.getElementById('input-1').value);
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: 'POST',
        url: "http://localhost:8080/api/update/receptionist",
        data: JSON.stringify({
            "id": receptionistID,
            "name": document.getElementById('input-1').value,
            "phone": '0000099923',
            "activated": 1
        }),
        success: function (result) {
            console.log(result);
        }
    })
}

function setDataForForm(entity) {
    console.log(entity);
    document.getElementById("input-1").value = entity.name;
    document.getElementById("input-2").value = entity.phone;
    document.getElementById("select-1").value = entity.activated;
}

(function () {
    var productionTable = $('#product-list');
    var productionForm = $('#product-add');
    var floatlabels = new FloatLabels('form', {
        // options go here
    });
})();
