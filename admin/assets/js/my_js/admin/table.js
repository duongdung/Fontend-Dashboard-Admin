var listTable = [];
var listTypeTable = [];
var typeTable = "";
var idTable="";

(function () {
  highLightForClick('click_table_mode');
  ajaxGetSerVer("http://localhost:8080/api/get/tables?typeseat=");
  ajaxGetTypeTable("http://localhost:8080/api/get/typetables?name=");
})();


function createTable(data) {
  let count = 0;
  let dataSource = data.map(function (table) {
    return [
      table.id,
      table.name,
      table.type_table_id,
      '<a class="cursor-pointer" onclick="showFormEdit(\'' + table.id + '\',\'' + table.name + '\',\'' + table.type_table_id + '\')"><i class="text-primary pe-7s-note"></i><a> /'
      + '<a class="cursor-pointer" onclick="deleteTable(\'' + table.id + '\')"><i class="text-primary pe-7s-close-circle"></i></a>'
    ];
  });
  data_table = $('#list_table').DataTable({
    destroy: true,
    scrollY: 450,
    scroller: true,
    data: dataSource,
    columns: [
      { title: "ID" },
      { title: "Name" },
      { title: "type table Id" },
      { title: "edit/update" }
    ]
  });
}


function showFormAdd() {
  setTypetable();
  showElement('form-table');
  hiddenElement('list-table');
  showElement('btn-table-add');
  hiddenElement('btn-table-edit');
}

function showTableList() {
  showElement('list-table');
  hiddenElement('form-table');
}

function showFormEdit(id, name, type_table_id) {
  idTable = id;
  document.getElementById('name_table').value = name;
  setTypetable();
  document.getElementById("option_type_table").value = type_table_id;
  showElement('form-table');
  hiddenElement('list-table');
  showElement('btn-table-edit');
  hiddenElement('btn-table-add');
}

// get set data for Form

function getTypeSeat() {
  let e = document.getElementById('option_type_table');
  typeTable = e.options[e.selectedIndex].value;
}

function setTypetable() {
  let select = document.getElementById('option_type_table');
  select.innerHTML="";
  for (let i = 0; i < listTypeTable.length; i++) {
    let option = document.createElement('OPTION');
    option.value = listTypeTable[i].id;
    option.appendChild(document.createTextNode(listTypeTable[i].type_seat));
    select.appendChild(option);
  }
}

function setDataForForm(type_table) {

}
function initializeTable() {
  return {
    "id" : idTable,
    "name": document.getElementById('name_table').value,
    "type_table_id": typeTable === "" ? document.getElementById("option_type_table").options[0].value : typeTable
  }
}


function sucsessFull(status) {
  alert(status);
  ajaxGetSerVer("http://localhost:8080/api/get/tables?typeseat=");
}

function resolveSuccess(status) {
  ajaxGetSerVer("http://localhost:8080/api/get/tables?typeseat=");
}

// CRUD Function

function insertTable() {
  ajaxPostSerVer("http://localhost:8080/api/insert/table", initializeTable());
}

function updateTable() {
  ajaxPostSerVer("http://localhost:8080/api/update/table", initializeTable());
}

function deleteTable(id) {
  ajaxDeleteTable("http://localhost:8080/api/delete/table?Id=" + id);
}


// Ajax code


function ajaxPostSerVer(url_post, table) {
  $.ajax({
    headers: {
      'Content-Type': 'application/json'
    },
    url: url_post,
    type: 'POST',
    data: JSON.stringify(table),
    success: function (status) {
      if (status === 'sucessfull') {
        resolveSuccess(status);
      } else {
        alert(status);
      }
    },
    error: function (result, status) {
      console.log(status);
    }
  })
}

function ajaxDeleteTable(url_post) {
  $.ajax({
    url: url_post,
    type: 'POST',
    success: function (status) {
      if (status === 'sucessfull') {
        sucsessFull('Delete Success Full');
      } else {
        alert(status);
      }
    },
    error: function (result, status) {
      console.log(status);
    }
  })
}


function ajaxGetSerVer(url_get) {
  $.ajax({
    headers: {
      'Accept': 'application/json',
    },
    url: url_get,
    type: 'GET',
    success: function (result) {
      listTable = [];
      if (result.length !== 0) {
        for (let i = 0; i < result.length; i++) {
          let itemTable = result[i];
          if (itemTable.activated === 1) {
            listTable.push(itemTable);
          }
        }
        createTable(listTable);
        showTableList();
      } else {
        alert("Not success Full");
      }
      //console.log(listTable);
    },
    error: function (result, status) {
      alert(result);
    }
  })
}

function ajaxGetTypeTable(url_get) {
  $.ajax({
    headers: {
      'Accept': 'application/json',
    },
    url: url_get,
    type: 'GET',
    success: function (result) {
      listTypeTable = [];
      for (let i = 0; i < result.length; i++) {
        let itemTable = result[i];
        if (itemTable.activated === 1) {
          listTypeTable.push(itemTable);
        }
      }
      //console.log(listTypeTable);
    },
    error: function (result, status) {
      console.log(status);
    }
  })
}