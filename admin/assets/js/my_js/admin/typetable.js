var tableList = [];
var typetable = {};
var data_table;
var condition_form = 0;
var id_typeTbl="";

(function () {
    var productionTable = $('#product-list');
    var productionForm = $('#product-add');
    var floatlabels = new FloatLabels('form', {});
    productionTable.click(function () {
        hiddenForm();
    });
    productionForm.click(function () {
        showElement('btn-type-table-add');
        hiddenElement('btn-type-table-edit');
        setTextForId('title-typetable','Add new type table');
        displayForm()
    });
    highLightForClick('click_type_table_mode');
})();

$(document).ready(function () {
    ajaxGetSerVer("http://localhost:8080/api/get/typetables?name=");
});

function createTypeTable(data) {
    let count = 0;
    let dataSource = data.map(function (tbl) {
        return [
            tbl.type_seat,
            tbl.type_room,
            tbl.description,
            tbl.price,
            '<a class="cursor-pointer" onclick="showFormEdit(' + count++ + ')"><i class="text-primary pe-7s-note"></i><a> /'
            + '<a class="cursor-pointer" onclick="deleteTypeTable(\''+tbl.id+'\')"><i class="text-primary pe-7s-close-circle"></i></a>'
        ];
    });
    data_table = $('#production-table').DataTable({
        destroy: true,
        scrollY: 450,
        scroller: true,
        data: dataSource,
        columns: [
            { title: "type seat" },
            { title: "type room" },
            { title: "description" },
            { title: "price" },
            { title: "edit/update" },
        ]
    });
}

// get set data for Form

function setDataForForm(type_table) {
    id_typeTbl = type_table.id;
    document.getElementById('tbl-type-seat').value = type_table.type_seat;
    document.getElementById('tbl-type-room').value = type_table.type_room;
    document.getElementById('tbl-description').value = type_table.description;
    document.getElementById('tbl-price').value = type_table.price;
}
function convertDataFromForm(id) {
    return {
        "id": id,
        "type_seat": document.getElementById('tbl-type-seat').value,
        "type_room": document.getElementById('tbl-type-room').value,
        "description": document.getElementById('tbl-description').value,
        "activated": 1,
        "price": document.getElementById('tbl-price').value,
    }

}
function displayForm() {
    $('.form').removeClass('d-none').addClass('d-block');
    $('.table').removeClass('d-block').addClass('d-none');
}
function hiddenForm() {
    $('.table').removeClass('d-none').addClass('d-block');
    $('.form').removeClass('d-block').addClass('d-none');
}

function sucsessFull(status) {
    alert(status);
    ajaxGetSerVer("http://localhost:8080/api/get/typetables?name=");
    hiddenForm();
}

function resolveSuccess(type_table){
    if (type_table.id === "none") {
        sucsessFull('add new successfull');
    } else {
        sucsessFull('update successfull');
    }
}

// Crud Function

function insertTypeTable() {
    typetable = {
        "id": "none",
        "type_seat": document.getElementById('tbl-type-seat').value,
        "type_room": document.getElementById('tbl-type-room').value,
        "description": document.getElementById('tbl-description').value.trim(),
        "price": document.getElementById('tbl-price').value
    }
    ajaxPostSerVer("http://localhost:8080/api/insert/typetable", typetable);

}

function updateTypeTable() {
    ajaxPostSerVer("http://localhost:8080/api/update/typetable", convertDataFromForm(id_typeTbl));
}

function showFormEdit(count) {
    setDataForForm(tableList[count]);
    showElement('btn-type-table-edit');
    hiddenElement('btn-type-table-add');
    setTextForId('title-typetable','Edit type table');
    displayForm();
}

function deleteTypeTable(id) {
    ajaxDeleteTypeTable("http://localhost:8080/api/delete/typetable?Id="+id);
}


// Ajax code


function ajaxPostSerVer(url_post, type_table) {
    $.ajax({
        headers: {
            'Content-Type': 'application/json'
        },
        url: url_post,
        type: 'POST',
        data: JSON.stringify(type_table),
        success: function (status) {
            if (status === 'sucessfull') {
                resolveSuccess(type_table);
            } else {
                alert(status);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    })
}

function ajaxDeleteTypeTable(url_post) {
    $.ajax({
        url: url_post,
        type: 'POST',
        success: function (status) {
            if (status === 'sucessfull') {
                sucsessFull('Delete Success Full');
            } else {
                alert(status);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    })
}

function ajaxGetSerVer(url_get) {
    $.ajax({
        headers: {
            'Accept': 'application/json',
        },
        url: url_get,
        type: 'GET',
        success: function (result) {
            tableList = [];
            for (let i = 0; i < result.length; i++) {
                let itemTable = result[i];
                if (itemTable.activated === 1) {
                    tableList.push(itemTable);
                }
            }
            createTypeTable(tableList);
        },
        error: function (result, status) {
            console.log(status);
        }
    })
}
