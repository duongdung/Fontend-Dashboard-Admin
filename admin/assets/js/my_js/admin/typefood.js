var typeFoodList = [];
var idTypeFood = "";
$(document).ready(function () {
    getTypeFood();
    highLightForClick('click_type_food_mode');
});

//Common function
function showFormDetail() {
    clearForm();
    setTextForId('title_type_food', 'Add new type Food')
    showElement('btn_add');
    hiddenElement('btn_edit');
    showElement('id_type_food_form');
    hiddenElement('id_type_food_table');
}

function showTableDetail() {
    hiddenElement('id_type_food_form');
    showElement('id_type_food_table');
}

function clearForm() {
    document.getElementById('id_name_type_food').value = "";
    document.getElementById('id_des_type_food').value = "";
    document.getElementById('id_unit_type_food').value = "";
}

function createTableProduct(data) {
    var dataSource = data.map(typefood => {
        console.log(typefood);
        return [
            typefood.id,
            typefood.name,
            typefood.description,
            typefood.unit,
            '<a class="cursor-pointer" onclick="editTypeFood(\'' + typefood.id + '\',\'' +
            typefood.name + '\',\'' + typefood.description + '\',\'' + typefood.unit + '\')">' +
            '<i class="text-success  pe-7s-note"></i>' +
            '</a> / ' +
            '<a class="cursor-pointer" onclick="deleteTypeFood(\'' + typefood.id + '\')">' +
            '<i class="text-success  pe-7s-close-circle"></i>' +
            '</a>'

        ];
    });
    $('#type_food_table').DataTable({
        destroy: true,
        ordering: true,
        scrollY: 390,
        scroller: true,
        data: dataSource,
        columns: [
            { title: "id" },
            { title: "name" },
            { title: "description" },
            { title: "unit" },
            { title: "edit/delete" },
        ]
    });
}

// For type Food

function insertTypeFood() {
    createTypeFood("http://localhost:8080/api/insert/typefood");
}

function updateTypeFood() {
    createTypeFood("http://localhost:8080/api/update/typefood");
}
function getTypeTable() {
    getTypeFood();
}

function deleteTypeFood(id) {
    ajaxDeleteTypeFood("http://localhost:8080/api/delete/typefood?Id=" + id);
}

function editTypeFood(id, name, descrip, unit) {
    showFormDetail();
    setTextForId('title_type_food', 'Edit type Food')
    showElement('btn_edit');
    hiddenElement('btn_add');
    idTypeFood = id;
    document.getElementById('id_name_type_food').value = name;
    document.getElementById('id_des_type_food').value = descrip;
    document.getElementById('id_unit_type_food').value = unit;
}


function initializeTypeFood() {
    return {
        id: idTypeFood,
        name: document.getElementById('id_name_type_food').value,
        description: document.getElementById('id_des_type_food').value,
        unit: document.getElementById('id_unit_type_food').value,
    }
}



// Resolve


// Ajax code

function getTypeFood() {
    $.ajax({
        headers: {
            accepts: "application/json",
        },
        type: 'GET',
        url: "http://localhost:8080/api/get/typefoods?name=",
        success: function (result) {
            typeFoodList = [];
            for (let i = 0; i < result.length; i++) {
                if(result[i].activated === 1) {
                    typeFoodList.push(result[i]);
                }
            }
            createTableProduct(typeFoodList);
            showTableDetail();
        },
        error: function (result,status) {
            console.log(status);
        }

    });
}


function createTypeFood(url_post) {
    $.ajax({
        headers: {
            "Content-Type": "application/json",
        },
        url: url_post,
        type: 'POST',
        data: JSON.stringify(initializeTypeFood()),
        success: function (result) {
            if (result === 'sucessfull') {
                alert("Sucessfull add/update")
                getTypeFood();
            } else {
                alert(result);
            }
        },
        error: function (result, status) {
            console.log("here" + status);
        }
    });
}

function ajaxDeleteTypeFood(url_post) {
    $.ajax({
        headers: {
            "Content-Type": "application/json",
        },
        url: url_post,
        type: 'POST',
        success: function (result) {
            if (result === 'sucessfull') {
                alert("Sucessfull Delete")
                getTypeFood();
            } else {
                alert(result);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    });
}