var foodList = [];
var typeFoodList = [];
var idTypeFood = "";
var typeFood = "";
var idFood = "";
var imageName = "";

$(document).ready(function () {
  getFoodFromDB();
  highLightForClick('click_food_mode');
});

//Common function

function createTableProduct(data) {
  var dataSource = data.map(function (food) {
    return [
      food.type_food_id,
      food.name,
      food.image,
      food.price,
      food.discount,
      '<a class="cursor-pointer" onclick="setDataFoodForm(\'' + food.id + '\',\'' +
      food.type_food_id + '\',\'' + food.name + '\',\'' + food.image + '\',\'' +
      food.price + '\',\'' + food.description + '\',\'' + food.discount + '\')">' +
      '<i class="text-success  pe-7s-note"></i>' +
      '</a> / ' +
      '<a class="cursor-pointer" onclick="deleteFood(\'' + food.id + '\')">' +
      '<i class="text-success  pe-7s-close-circle"></i>' +
      '</a>'
    ];
  });
  $('#production-table').DataTable({
    destroy: true,
    ordering: true,
    scrollY: 390,
    scroller: true,
    data: dataSource,
    columns: [
      { title: "Type" },
      { title: "Name" },
      { title: "Image" },
      { title: "Price" },
      { title: "discount" },
      { title: "Edit/Delete" }
    ]
  });
}

function showFormDetail() {
  getTypeFood();
  setTextForId('title-food', 'ADD NEW FOOD');
  showElement('btn-add-food');
  hiddenElement('btn-update-food');
  showElement('id_food_form');
  hiddenElement('id_food_table');
  document.getElementById("image-food").src="";
}

function showEditFood() {
  setTextForId('title-food', 'UPDATE FOOD');
  showElement('btn-update-food');
  hiddenElement('btn-add-food');
  showElement('id_food_form');
  hiddenElement('id_food_table');
}



function showTableDetail() {
  showElement('id_food_table');
  hiddenElement('id_food_form');
}

function setDataFoodForm(id, idtype, name, image, price, description, discount) {
  idFood = id;
  idTypeFood = idtype;
  imageName = image;
  setDataForSelectType(idtype);
  document.getElementById('id_type_food').value = idtype;
  document.getElementById('id_name_food').value = name;
  document.getElementById('id_price').value = price;
  document.getElementById('image-food').src = "http://localhost:8080/api/image/food/" + image;
  document.getElementById('id_desscription').value = description;
  document.getElementById('id_discount').value = discount;
  showEditFood();
}



function getNameForSelect() {

}
// For Food CRUD

function insertFood() {
  createFood("http://localhost:8080/api/insert/food");
  if (document.getElementById("files-food").files[0].name !== null) {
    createImageInServer("http://localhost:8080/api/create/image/food");
  }
}

function updateFood() {
  createFood("http://localhost:8080/api/update/food");
  if (document.getElementById("files-food").files[0].name !== null) {
    createImageInServer("http://localhost:8080/api/create/image/food");
  }
}

function deleteFood(id) {
  deleteFoodInDB("http://localhost:8080/api/delete/food?Id=" + id);
}

function getTypeTable() {
  getTypeFood();
}


// Resolve
function getImageFood() {
  document.getElementById('files-food').click();
}

function selectImage() {
  let imageElement = document.getElementById('image-food');
  let imageFile = document.getElementById('files-food').files[0];
  if (imageFile !== null) {
    imageName = imageFile.name;
  }
  let reader = new FileReader();
  reader.addEventListener('load', function () {
    imageElement.src = reader.result;
  }, false)
  reader.readAsDataURL(imageFile);
}


function initializeFood() {
  let foodInit = {
    "id": idFood,
    "type_food_id": idTypeFood,
    "name": document.getElementById("id_name_food").value,
    "image": document.getElementById("files-food").files[0].name,
    "description": document.getElementById("id_desscription").value,
    "price": document.getElementById("id_price").value,
    "discount": document.getElementById("id_discount").value
  }
  return foodInit;
}

function setDataForSelectType() {
  let option = "";
  for (let i = 0; i < typeFoodList.length; i++) {
    option += '<option value="' + typeFoodList[i].id + '" >' + typeFoodList[i].name + '</option>';
  }
  $("#id_type_food").html(option);
}

function getIdForSelect() {
  idTypeFood = document.getElementById('id_type_food').value;
}

// Ajax code


function getFoodFromDB() {
  $.ajax({
    headers: {
      accepts: "application/json",
    },
    type: 'GET',
    url: "http://localhost:8080/api/get/foods?name=",
    success: function (result) {
      foodList = [];
      for (let i = 0; i < result.length; i++) {
        if (result[i].activated === 1) {
          foodList.push(result[i]);
        }
      }
      createTableProduct(foodList);
      getTypeFood();
      showTableDetail();
    },
    error: function (result, status) {
      console.log(status);
    }

  });
}

function getTypeFood() {
  $.ajax({
    headers: {
      accepts: "application/json",
    },
    type: 'GET',
    url: "http://localhost:8080/api/get/typefoods?name=",
    success: function (result) {
      typeFoodList = result;
      let option = "";
      for (let i = 0; i < result.length; i++) {
        option += '<option value="' + result[i].id + '" >' + result[i].name + '</option>';
      }
      $("#id_type_food").html(option);
    },
    error: function (status) {
      console.log(result);
    }
  });

}

function deleteFoodInDB(url_post) {
  $.ajax({
    headers: {
      "Content-Type": "application/json",
    },
    type: 'POST',
    url: url_post,
    success: function (result) {
      if (result === 'sucessfull') {
        alert(result);
        getFoodFromDB();
      } else {
        alert(result);
      }
    },
    error: function (result, status) {
      console.log(status, result);
    }
  });
}

function createFood(url_post) {
  $.ajax({
    headers: {
      "Content-Type": "application/json",
    },
    type: 'POST',
    url: url_post,
    data: JSON.stringify(initializeFood()),
    success: function (result) {
      if (result === 'sucessfull') {
        alert(result);
        getFoodFromDB();
      } else {
        alert(result);
      }
    },
    error: function (result, status) {
      console.log(status, result);
    }
  });
}

function createImageInServer(url_post) {
  event.preventDefault();
  let formData = new FormData();
  formData.append('imageUpload', document.getElementById('files-food').files[0]);
  $.ajax({
    type: 'POST',
    url: url_post,
    data: formData,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    success: function (result) {
      console.log(result);
    },
    error: function (result, status) {
      console.log(result);
    }
  })
}