var serviceList = [];
var idService = "";
$(document).ready(function () {
    getService();
});

//Common function
function showFormDetail() {
    clearForm();
    setTextForId('title_service', 'Thêm Mới Dịch Vụ')
    showElement('btn_add');
    hiddenElement('btn_edit');
    showElement('id_service_form');
    hiddenElement('id_service_table');
}

function showTableDetail() {
    hiddenElement('id_service_form');
    showElement('id_service_table');
}

function clearForm() {
    document.getElementById('id_name_service').value = "";
    document.getElementById('id_des_service').value = "";
    document.getElementById('id_image_service').value = "";
    document.getElementById('id_discount_service').value = "";

}

function createTableProduct(data) {
    var dataSource = data.map(service => {
        console.log(service);
        return [
            service.id,
            service.name,
            service.description,
            service.discount,
            service.image,
            '<a class="cursor-pointer" onclick="editService(\'' + service.id + '\',\'' +
            service.name + '\',\'' + service.description + '\',\'' + service.discount + '\',\'' +
            service.image + '\')">' + '<i class="text-success  pe-7s-note"></i></a> / ' +
            '<a class="cursor-pointer" onclick="deleteService(\'' + service.id + '\')">' +
            '<i class="text-success  pe-7s-close-circle"></i>' +
            '</a>'

        ];
    });
    $('#service_table').DataTable({
        destroy: true,
        ordering: true,
        scrollY: 390,
        scroller: true,
        data: dataSource,
        columns: [
            { title: "id" },
            { title: "name" },
            { title: "description" },
            { title: "discount" },
            { title: "image" },
            { title: "Edit / Delete" }
        ]
    });
}

// For type Food

function insertService() {
    createService("http://localhost:8080/api/insert/service");
}

function updateService() {
    createService("http://localhost:8080/api/update/service");
}
function getTypeTable() {
    getService();
}

function deleteService(id) {
    ajaxDeleteService("http://localhost:8080/api/delete/service?Id=" + id);
}

function editService(id, name, descrip, discount, image) {
    showFormDetail();
    setTextForId('title_service', 'Update Service')
    showElement('btn_edit');
    hiddenElement('btn_add');
    idService = id;
    document.getElementById('id_name_service').value = name;
    document.getElementById('id_des_service').value = descrip;
    document.getElementById('id_image_service').value = image;
    document.getElementById('id_discount_service').value = discount;
}


function initializeService() {
    return {
        id: idService,
        name: document.getElementById('id_name_service').value.trim(),
        description: document.getElementById('id_des_service').value.trim(),
        discount: document.getElementById('id_discount_service').value.trim(),
        image: document.getElementById('id_image_service').value.trim()
    }
}



// Resolve


// Ajax code

function getService() {
    $.ajax({
        headers: {
            accepts: "application/json",
        },
        type: 'GET',
        url: "http://localhost:8080/api/get/services?name=",
        success: function (result) {
            serviceList = [];
            for (let i = 0; i < result.length; i++) {
                if (result[i].activated === 1) {
                    serviceList.push(result[i]);
                }
            }
            createTableProduct(serviceList);
            showTableDetail();
        },
        error: function (result, status) {
            console.log(status);
        }

    });
}


function createService(url_post) {
    $.ajax({
        headers: {
            "Content-Type": "application/json",
        },
        url: url_post,
        type: 'POST',
        data: JSON.stringify(initializeService()),
        success: function (result) {
            if (result === 'sucessfull') {
                alert("Sucessfull add/update")
                getService();
            } else {
                alert(result);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    });
}

function ajaxDeleteService(url_post) {
    $.ajax({
        headers: {
            "Content-Type": "application/json",
        },
        url: url_post,
        type: 'POST',
        success: function (result) {
            if (result === 'sucessfull') {
                alert("Sucessfull Delete")
                getService();
            } else {
                alert(result);
            }
        },
        error: function (result, status) {
            console.log(status);
        }
    });
}