function hiddenElement(id) {
  document.getElementById(id).style.display = 'none';
}

function showElement(id) {
  document.getElementById(id).style.display = 'block';
}

function setTextForId(id, text) {
  document.getElementById(id).innerHTML = text;
}

function highLightForClick(id) {
  document.getElementById(id).style.color = "red";
}

function hiddenhighLightForClick(id) {
  document.getElementById(id).style.color = "white";
}

function showManageFood() {
  location.assign('food.html');
}

function showManageTypeFood() {
  location.assign('typefood.html');
}

function showManageTable() {
  location.assign('table.html');
}

function showManageTypeTable() {
  location.assign('typetable.html');
}

// Defind image when click

function showImage() {
  let image = document.getElementById('image-food');
  let file = document.getElementById('id_image').files[0];
  let reader = new FileReader();
  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);
  reader.readAsDataURL(file);
  console.log("done");
}