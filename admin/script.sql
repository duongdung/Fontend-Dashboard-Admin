USE [ASM]
GO
/****** Object:  Table [dbo].[tblAdmin]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAdmin](
	[id] [nvarchar](50) NOT NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[permission] [nvarchar](50) NULL,
	[activated] [int] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblBooking]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBooking](
	[id] [nvarchar](50) NOT NULL,
	[customer_name] [nvarchar](50) NULL,
	[customer_phone] [int] NULL,
	[customer_email] [nvarchar](50) NULL,
	[customer_number] [int] NULL,
	[table_id] [nvarchar](50) NULL,
	[service_id] [nvarchar](50) NULL,
	[date_book] [datetime] NULL,
	[status] [int] NULL,
	[activated] [int] NULL,
 CONSTRAINT [PK_tblBooking] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCode]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCode](
	[code] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblFood]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFood](
	[id] [nvarchar](50) NOT NULL,
	[type_food_id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[image] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[price] [float] NULL,
	[activated] [int] NULL,
	[discount] [int] NULL,
 CONSTRAINT [PK_tblFood] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSaveId]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSaveId](
	[nameId] [nvarchar](20) NULL,
	[value] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblService]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblService](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
	[discount] [int] NULL,
	[image] [nvarchar](50) NULL,
	[activated] [int] NULL,
 CONSTRAINT [PK_tblService] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTable]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTable](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[type_table_id] [nvarchar](50) NULL,
	[activated] [int] NULL,
 CONSTRAINT [PK_tblTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTypeFood]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTypeFood](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](100) NULL,
	[unit] [nvarchar](50) NULL,
	[activated] [int] NULL,
 CONSTRAINT [PK_tblTypeFood] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTypeTable]    Script Date: 3/13/2018 8:29:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTypeTable](
	[id] [nvarchar](50) NOT NULL,
	[type_seat] [nvarchar](50) NULL,
	[type_room] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
	[activated] [int] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_tblTypeTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tblBooking]  WITH CHECK ADD FOREIGN KEY([table_id])
REFERENCES [dbo].[tblTable] ([id])
GO
ALTER TABLE [dbo].[tblBooking]  WITH CHECK ADD  CONSTRAINT [FK_tblBooking_tblService] FOREIGN KEY([service_id])
REFERENCES [dbo].[tblService] ([id])
GO
ALTER TABLE [dbo].[tblBooking] CHECK CONSTRAINT [FK_tblBooking_tblService]
GO
ALTER TABLE [dbo].[tblFood]  WITH CHECK ADD  CONSTRAINT [FK_tblFood_tblTypeFood] FOREIGN KEY([type_food_id])
REFERENCES [dbo].[tblTypeFood] ([id])
GO
ALTER TABLE [dbo].[tblFood] CHECK CONSTRAINT [FK_tblFood_tblTypeFood]
GO
ALTER TABLE [dbo].[tblTable]  WITH CHECK ADD FOREIGN KEY([type_table_id])
REFERENCES [dbo].[tblTypeTable] ([id])
GO
